module.exports = {
  arrowParens: "always",
  printWidth: 80,
  quoteProps: "consistent",
  semi: true,
  singleQuote: true,
  tabWidth: 4,
  trailingComma: "all",
};
