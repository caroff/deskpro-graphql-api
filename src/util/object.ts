export const objlen = (obj: object): number => Object.keys(obj).length;
